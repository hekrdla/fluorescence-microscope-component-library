This repository offers a set of free vector graphic (*.svg) components for easy drawing of microscopy setups e.g. in [inkscape](https://inkscape.org/) or Adobe Illustrator.

<div align="center">
<img src=https://gitlab.com/hekrdla/fluorescence-microscope-component-library/-/raw/main/microscopy_components_v3.png width="900">
</div>

The library of microscopy components has been used to sketch our custom-build super-resolution microscopy setup:

<div align="center">
<img src=https://gitlab.com/hekrdla/fluorescence-microscope-component-library/-/raw/main/microscope_213_v3_2024_05_09.png width="1000">
</div>

## Acknowledgement

The components are based on `componentLibrary` project http://www.gwoptics.org/ComponentLibrary/ by Alexander Franzen with CC BY-NC 3.0 licence supplied by the following statement:

> We explicitly waive the requirement for attribution when the library is used to create a small number of illustrations for a new work, such as a research paper or a web page. However in all other cases,  for example, when the library or its components are distributed, or if the library is used to illustrate major parts of a larger work, such as a book or PhD thesis, the creator Alexander Franzen must be acknowledged as specified in by the Creative Commons license above.

